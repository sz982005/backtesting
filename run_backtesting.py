from backtesting.cta_backtesting import BacktestingEngine
from strategies.bolling_strategy import BollingStrategy

engine = BacktestingEngine()
#设置回测参数
engine.set_parameters(
    symbol="BTCUSDT",
    exchange="BINANCE",
    interval="1m",
    start= "2019-09-01 00:00:00",
    end= "2019-10-01 00:00:00",
    rate=0.001,
    slippage=0,
    size=1,
    capital=10000,
    backtest_period = ["15T"]
)
#载入回测数据
engine.load_data()
#初始化回测参数
engine.init_backtesting()
#添加回测策略和参数
engine.add_strategy(BollingStrategy,"30T",strategy_para=[100, 2, 5])
#执行回测程序
engine.run_backtesting()
#计算每日收益
engine.calculate_result()
#计算策略统计数据
engine.calculate_statistics()
#展示并保存图形
engine.show_chart()
#输出交易明细
engine.output_trades()
#输出K线图
#engine.show_bar_chart()






